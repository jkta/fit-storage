package com.jkta.fitstorage.services

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.User

/**
 * Created by Joanna Kuta on 09.03.2020.
 */

object UserService {

    var user: FirebaseUser? = null
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var ingredientList = arrayListOf<Ingredient>()

    fun initUserFromFirebaseAuth() {
        if(firebaseAuth.currentUser != null) {
            user = firebaseAuth.currentUser
        }
    }

    fun signOutUser() {
        firebaseAuth.signOut()
    }

    fun addAllToIngredientList(ingredients: ArrayList<Ingredient>) {
        ingredientList.clear();
        ingredientList.addAll(ingredients)
    }

    fun getMyIngredients(): ArrayList<Ingredient> {
        return ingredientList
    }

    fun getUserName(): String {
        return user!!.email!!
    }

    fun getUserUid(): String {
        return user!!.uid
    }
}