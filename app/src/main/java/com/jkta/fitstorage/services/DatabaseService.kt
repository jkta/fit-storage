package com.jkta.fitstorage.services

import com.google.firebase.firestore.FirebaseFirestore
import com.jkta.fitstorage.models.Ingredient
import io.reactivex.Observable

/**
 * Created by Joanna Kuta on 23.03.2020.
 */

object DatabaseService {

    private val db = FirebaseFirestore.getInstance()

    fun addNewIngredient(ingredient: Ingredient): Observable<Boolean> {
        return Observable.create { emitter ->
            db.collection("ingredients").document().set(ingredient).addOnSuccessListener {
                emitter.onNext(true)
            }.addOnFailureListener {
                emitter.onError(Throwable(it.message))
            }
        }
    }
}