package com.jkta.fitstorage.helpers

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.jkta.fitstorage.R
import com.jkta.fitstorage.models.Ingredient

/**
 * Created by Joanna Kuta on 23.03.2020.
 */

object Dialog {

    fun addIngredientPopup(context: Context,
                           acceptButtonListener: (Ingredient, Boolean) -> Unit): Dialog {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_add_ingredient)

        val nameEt = dialog.findViewById<EditText>(R.id.editTextName)
        val amountEt = dialog.findViewById<EditText>(R.id.editTextAmount)
        val unitEt = dialog.findViewById<EditText>(R.id.editTextUnit)
        val okButton = dialog.findViewById<Button>(R.id.popupBtnLeft)
        val cancelButton = dialog.findViewById<Button>(R.id.popupButtonRight)

        okButton.setOnClickListener {
            val ingredientName = nameEt.text.toString()
            val ingredientAmount = amountEt.text.toString()
            val ingredientUnit = unitEt.text.toString()

            if(ingredientsFieldsOk(name = ingredientName, amount = ingredientAmount, unit = ingredientUnit)) {
                acceptButtonListener(createIngredient(ingredientName, ingredientAmount, ingredientUnit), true)
                dialog.dismiss()
            } else {
                acceptButtonListener(Ingredient(), false)
            }
        }

        cancelButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp

        return dialog
    }

    fun ingredientsFieldsOk(name: String, amount: String, unit: String): Boolean {
        val amountValue = amount.toDoubleOrNull()
        return name.isNotEmpty() && amount.isNotEmpty() && amountValue !== null && unit.isNotEmpty()
    }

    private fun createIngredient(name: String, amount: String, unit: String): Ingredient {
        return Ingredient(name, amount.toDouble(), unit)
    }
}