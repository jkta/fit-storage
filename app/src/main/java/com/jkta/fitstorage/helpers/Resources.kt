package com.jkta.fitstorage.helpers

import android.graphics.drawable.Drawable
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.jkta.fitstorage.R
import com.jkta.fitstorage.main.App

/**
 * Created by Joanna Kuta on 01.02.2020.
 */

object Resources {
    fun color(@ColorRes id: Int): Int = ContextCompat.getColor(App.instance, id)
    fun string(@StringRes id: Int): String = App.instance.getString(id)
    fun array(@ArrayRes id: Int): Array<out String>? = App.instance.resources.getStringArray(id)
    fun getDrawable(@DrawableRes id: Int): Drawable? = ContextCompat.getDrawable(App.instance, id)
}