package com.jkta.fitstorage.helpers

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * Created by Joanna Kuta on 20.01.2020.
 */

var toast: Toast? = null

fun AppCompatActivity.toast(text: String) {
    toast?.cancel()
    toast = Toast.makeText(this, text, Toast.LENGTH_SHORT)
    toast?.show()
}

fun Fragment.toast(text: String) {
    toast?.cancel()
    toast = Toast.makeText(activity, text, Toast.LENGTH_SHORT)
    toast?.show()
}