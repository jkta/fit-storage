package com.jkta.fitstorage.helpers

import android.content.Intent
import com.jkta.fitstorage.main.App
import com.jkta.fitstorage.main.authorization.AuthActivity
import com.jkta.fitstorage.main.mainNavigation.MainNavigationActivity

/**
 * Created by Joanna Kuta on 01.02.2020.
 */

object Router {
    fun goToMainActivity() {
        val intent = Intent(App.instance, MainNavigationActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        App.instance.startActivity(intent)
    }

    fun goToLoginActivity() {
        val intent = Intent(App.instance, AuthActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        App.instance.startActivity(intent)
    }
}