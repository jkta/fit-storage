package com.jkta.fitstorage.adapters

/**
 * Created by Joanna Kuta on 05.02.2020.
 */

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jkta.fitstorage.R
import com.jkta.fitstorage.models.Recipe
import kotlinx.android.synthetic.main.item_recently_added.view.*
import java.util.ArrayList;


class HomeVerticalRecommendedAdapter(val context: Context, val foodList: ArrayList<Recipe>):  RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_recently_added, parent, false)
        return FoodItemsViewHolder(view, context)
    }

    override fun getItemCount(): Int {
        return foodList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val recipeHolder = (holder as FoodItemsViewHolder)
        recipeHolder.bind((foodList[position]))
    }

    class FoodItemsViewHolder(rowView: View, context: Context): RecyclerView.ViewHolder(rowView) {
        val view: View = rowView

        fun bind(food: Recipe) {
            view.itemTitleTextView.text = food.title
            view.itemAboutTextView.text = food.recipeDescription

        }
    }
}