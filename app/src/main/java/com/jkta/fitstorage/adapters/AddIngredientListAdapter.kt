package com.jkta.fitstorage.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jkta.fitstorage.R
import com.jkta.fitstorage.models.Ingredient
import kotlinx.android.synthetic.main.item_ingredient_add.view.*

/**
 * Created by Joanna Kuta on 13.02.2020.
 */


class AddIngredientListAdapter(val context: Context, var ingredientList: ArrayList<Ingredient>):  RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ingredient_add, parent, false)
        return FoodItemsViewHolder(view, context)
    }

    override fun getItemCount(): Int {
        return ingredientList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val recipeHolder = (holder as FoodItemsViewHolder)
        recipeHolder.bind((ingredientList[position]), position)
    }

    class FoodItemsViewHolder(rowView: View, context: Context): RecyclerView.ViewHolder(rowView) {
        val view: View = rowView

        fun bind(ingredient: Ingredient, position: Int) {

            view.itemIngredientNameTextView.text = ingredient.name
            view.itemAmountTextView.text = ingredient.amount.toString()
            view.itemMeasureTextView.text = ingredient.unit
        }
    }
}