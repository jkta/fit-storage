package com.jkta.fitstorage.adapters

/**
 * Created by Joanna Kuta on 05.02.2020.
 */

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Dialog
import com.jkta.fitstorage.helpers.toast
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe
import com.jkta.fitstorage.services.DatabaseService
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.item_food_horizontal.view.*
import kotlinx.android.synthetic.main.item_main_add_food.view.*
import java.util.*

interface IngredientAddedListener {
    fun onAddListener()
}

class HomeHorizontalListAdapter(private val context: Context, val ingredientList: ArrayList<Ingredient>, val onAddedListener: IngredientAddedListener):  RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_CELL = 1
    private val VIEW_TYPE_FOOTER = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            if(viewType == VIEW_TYPE_CELL) {
                LayoutInflater.from(context).inflate(R.layout.item_food_horizontal, parent, false)
            } else {
                LayoutInflater.from(context).inflate(R.layout.item_main_add_food, parent, false)
            }

        return FoodItemsViewHolder(view, context)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == ingredientList.size) VIEW_TYPE_FOOTER else VIEW_TYPE_CELL
    }

    override fun getItemCount(): Int {
        return ingredientList.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val recipeHolder = (holder as FoodItemsViewHolder)
        if(position == ingredientList.size) {
            recipeHolder.bindFooterButton(onAddedListener)
        } else {
            recipeHolder.bind((ingredientList[position]))
        }
    }

    class FoodItemsViewHolder(rowView: View, context: Context): RecyclerView.ViewHolder(rowView) {
        val view: View = rowView
        private val appContext = context

        fun bind(food: Ingredient) {
            view.itemName.text = food.name
            val itemAmountText = "%.2f".format(food.amount) + " " + food.unit
            view.itemAmount.text = itemAmountText
        }

        fun bindFooterButton(onAddedListener: IngredientAddedListener) {
            view.fabAddFood.setOnClickListener {
                Dialog.addIngredientPopup(appContext) { ingredient, isCorrect ->
                    addNewIngredient(ingredient, isCorrect, appContext, onAddedListener)
                }
            }
        }

        private fun addNewIngredient(ingredient: Ingredient, isCorrect: Boolean, context: Context, onAddedListener: IngredientAddedListener) {
            if(isCorrect) {
                DatabaseService.addNewIngredient(ingredient).subscribeBy(onNext = {isSuccess ->
                    if(isSuccess) {
                        Toast.makeText(context, "Adding ingredient success",Toast.LENGTH_SHORT).show()
                        onAddedListener.onAddListener()
                    }
                },
                onError = {
                    Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                })
            } else {
                Toast.makeText(context, "Adding ingredient error",Toast.LENGTH_SHORT).show()
            }
        }
    }
}