package com.jkta.fitstorage.models

/**
 * Created by Joanna Kuta on 09.03.2020.
 */

class User (
    val email: String,
    val firstName: String,
    val lastName: String) {

}