package com.jkta.fitstorage.models

import java.util.*

/**
 * Created by Joanna Kuta on 18.02.2020.
 */

open class Recipe(
    val title: String = "",
    val ingredients: List<Ingredient> = listOf(),
    val recipeDescription: String = "",
    val createdByUid: String = "",
    val createdByUser: String = "",
    val createdAt: Date = Date(),
    var percentMatch: String = ""
)