package com.jkta.fitstorage.models

import com.jkta.fitstorage.services.UserService

/**
 * Created by Joanna Kuta on 13.02.2020.
 */

open class Ingredient (
    open val name: String = "",
    open val amount: Double = 0.0,
    open val unit: String = "",
    val createdByUid: String = UserService.getUserUid()
)