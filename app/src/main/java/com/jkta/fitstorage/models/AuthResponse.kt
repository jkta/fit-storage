package com.jkta.fitstorage.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Joanna Kuta on 30.01.2020.
 */

data class AuthResponse(
    val isSuccess: Boolean,
    val message: String
)