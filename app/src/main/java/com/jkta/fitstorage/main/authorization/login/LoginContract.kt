package com.jkta.fitstorage.main.authorization.login

import com.jkta.fitstorage.models.AuthResponse
import io.reactivex.Observable

/**
 * Created by Joanna Kuta on 31.01.2020.
 */
interface LoginContract {

    interface View {
    }

    interface Presenter {
        fun loginUserWithEmailAndPassword(email: String, password: String): Observable<AuthResponse>
    }
}