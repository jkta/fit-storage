package com.jkta.fitstorage.main.mainNavigation.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jkta.fitstorage.R
import com.jkta.fitstorage.adapters.AddIngredientListAdapter
import com.jkta.fitstorage.helpers.toast
import com.jkta.fitstorage.models.Ingredient
import kotlinx.android.synthetic.main.fragment_main_add.*

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class AddFragment: Fragment(), AddContract.View {

    var presenter: AddPresenter? = null
    private val ingredientsArray = arrayListOf<Ingredient>()
    private var adapter: AddIngredientListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_add, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = AddPresenter(this)
        setupIngredientButtonListener()
        setupRecycler()
        setupSaveButtonListener()
    }

    override fun setupRecycler() {
        ingredientRecyclerView.setHasFixedSize(true)
        ingredientRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter = AddIngredientListAdapter(context!!, ingredientsArray)
        ingredientRecyclerView.adapter = adapter
        ingredientRecyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun showToastMessage(message: String) {
        toast(message)
    }

    private fun setupSaveButtonListener() {
        saveRecipeButton.setOnClickListener {
            //throw Error("test")
            presenter?.saveRecipe(editText.text.toString(), editText2.text.toString())
        }
    }

    private fun setupIngredientButtonListener() {
        ingredientAddButton.setOnClickListener {
            addIngredient()
            clearIngredientEditText()
            setFocusToIngredientNameEditText()
        }
    }

    private fun addIngredient() {
        val ingredientName = ingredientNameEditText.text.toString()
        val ingredientAmount = ingredientAmountEditText.text.toString().toDouble()
        val ingredientUnit = ingredientUnitEditText.text.toString()

        presenter!!.addIngredientToList(ingredientName, ingredientAmount, ingredientUnit)
        adapter!!.ingredientList = presenter!!.getIngredientsList()
        adapter!!.notifyDataSetChanged()
    }

    private fun clearIngredientEditText() {
        ingredientNameEditText.text.clear()
        ingredientAmountEditText.text.clear()
        ingredientUnitEditText.text.clear()
    }

    private fun setFocusToIngredientNameEditText(){
        ingredientNameEditText.requestFocus()
    }
}