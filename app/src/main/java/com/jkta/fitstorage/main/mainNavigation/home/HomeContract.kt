package com.jkta.fitstorage.main.mainNavigation.home

import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

interface HomeContract {

    interface View {
        fun setupRecycler()
        fun setupHorizontalRecycler()
        fun setRecipeData(recipeList: List<Recipe>)
        fun setIngredientData(ingredientList: List<Ingredient>)
    }

    interface Presenter {
        fun getRecipeList(): ArrayList<Recipe>
        fun reloadIngredientList()
        fun getIngredientList(): ArrayList<Ingredient>
    }
}