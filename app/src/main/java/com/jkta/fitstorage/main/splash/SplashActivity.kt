package com.jkta.fitstorage.main.splash

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.jkta.fitstorage.R
import com.jkta.fitstorage.main.authorization.AuthActivity
import com.jkta.fitstorage.main.mainNavigation.MainNavigationActivity
import com.jkta.fitstorage.services.UserService
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*
import kotlin.concurrent.schedule


/**
 * Created by Joanna Kuta on 19.02.2020.
 */

class SplashActivity: AppCompatActivity(), SplashContract.View {

    private var presenter: SplashPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter = SplashPresenter(this)

        Timer("pick activity", false).schedule(2500) {
            runOnUiThread {
                val intent: Intent = if (UserService.user != null) {
                    Intent(this@SplashActivity, MainNavigationActivity::class.java)
                } else {
                    Intent(this@SplashActivity, AuthActivity::class.java)
                }
                val options = ActivityOptions.makeSceneTransitionAnimation(this@SplashActivity, imageLogo as View, "appIcon")
                startActivity(intent, options.toBundle())
            }
        }
    }
}