package com.jkta.fitstorage.main.authorization

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R
import com.jkta.fitstorage.main.authorization.login.LoginFragment
import com.jkta.fitstorage.main.authorization.register.RegisterFragment
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.fragment_authentication_login.*

/**
 * Created by Joanna Kuta on 30.01.2020.
 */

class AuthActivity : AppCompatActivity(), AuthContract.View {

    private var presenter: AuthPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        presenter = AuthPresenter(this)
        setupFragment(LoginFragment())
        setRegisterClick()
    }

    override fun setupFragment(fragment: Fragment, backstack: Boolean) {
        val manager = supportFragmentManager
        val fragmentTransaction = manager.beginTransaction()
        if(backstack) {
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
        }
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
        manager.executePendingTransactions()
    }

    override fun setRegisterClick() {
        registerText.setOnClickListener {
            setupFragment(RegisterFragment(), true)
        }
    }
}