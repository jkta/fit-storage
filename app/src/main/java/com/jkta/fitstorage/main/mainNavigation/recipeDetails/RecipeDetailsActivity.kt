package com.jkta.fitstorage.main.mainNavigation.recipeDetails

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.jkta.fitstorage.R

/**
* Created by Joanna Kuta on 31/03/2020
*/

class RecipeDetailsActivity: AppCompatActivity(), RecipeDetailsContract.View {

    var presenter = RecipeDetailsPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_details)
    }
}