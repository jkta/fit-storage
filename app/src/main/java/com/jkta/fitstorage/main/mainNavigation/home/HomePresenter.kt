package com.jkta.fitstorage.main.mainNavigation.home

import com.google.firebase.firestore.FirebaseFirestore
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe
import com.jkta.fitstorage.services.UserService

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class HomePresenter(_view: HomeContract.View): HomeContract.Presenter {

    private var recipes = arrayListOf<Recipe>()
    private var ingredients = arrayListOf<Ingredient>()
    private val homeView = _view
    private val db = FirebaseFirestore.getInstance()

    init {
        getRecipesFromDatabase()
        getIngredientsFromDatabase()
    }

    override fun getRecipeList(): ArrayList<Recipe> {
       return recipes
    }

    override fun reloadIngredientList() {
        getIngredientsFromDatabase()
    }

    override fun getIngredientList(): ArrayList<Ingredient> {
        return ingredients
    }

    private fun getRecipesFromDatabase() {
        db.collection("recipes").get().addOnSuccessListener { document ->
            if(document != null) {
                val data = document.toObjects(Recipe::class.java)
                recipes.addAll(data)
                homeView.setRecipeData(recipes.toList())
            }
        }
    }

    private fun getIngredientsFromDatabase() {
        val ingredientsRef = db.collection("ingredients")
        val query = ingredientsRef.whereEqualTo("createdByUid", UserService.getUserUid())
        query.get().addOnSuccessListener { document ->
            if(document != null) {
                val data = document.toObjects(Ingredient::class.java)
                ingredients.clear()
                ingredients.addAll(data)
                UserService.addAllToIngredientList(ingredients)
                homeView.setIngredientData(ingredients.toList())
            }
        }
    }
}