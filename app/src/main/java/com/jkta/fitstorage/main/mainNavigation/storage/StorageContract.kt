package com.jkta.fitstorage.main.mainNavigation.storage

import com.jkta.fitstorage.models.Recipe

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

interface StorageContract {

    interface View {
        fun setRecipeData(recipeList: List<Recipe>)
    }

    interface Presenter {
        fun getRecipeList(): ArrayList<Recipe>
    }
}