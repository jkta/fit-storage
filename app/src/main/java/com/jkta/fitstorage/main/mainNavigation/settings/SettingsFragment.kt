package com.jkta.fitstorage.main.mainNavigation.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.toast
import kotlinx.android.synthetic.main.fragment_main_settings.*

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class SettingsFragment: Fragment(), SettingsContract.View {

    private var presenter: SettingsPresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = SettingsPresenter(this)
        prepareLogoutListener()
    }

    private fun prepareLogoutListener() {
        logoutButton.setOnClickListener {
            presenter?.signOut()
            toast("User signed out")
        }
    }
}