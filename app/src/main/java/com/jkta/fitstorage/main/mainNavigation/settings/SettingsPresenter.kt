package com.jkta.fitstorage.main.mainNavigation.settings

import com.jkta.fitstorage.helpers.Router
import com.jkta.fitstorage.services.UserService

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class SettingsPresenter(_view: SettingsContract.View): SettingsContract.Presenter {

    override fun signOut() {
        UserService.signOutUser()
        Router.goToLoginActivity()
    }
}
