package com.jkta.fitstorage.main.authorization

import androidx.fragment.app.Fragment
import com.jkta.fitstorage.models.AuthResponse
import io.reactivex.Observable

/**
 * Created by Joanna Kuta on 30.01.2020.
 */

public interface AuthContract {

    interface View {
        fun setupFragment(fragment: Fragment, backstack: Boolean = false)
        fun setRegisterClick()
    }

    interface Presenter {

    }
}