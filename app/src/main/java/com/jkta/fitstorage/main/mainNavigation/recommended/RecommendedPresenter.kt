package com.jkta.fitstorage.main.mainNavigation.recommended

import com.google.firebase.firestore.FirebaseFirestore
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe
import com.jkta.fitstorage.services.UserService
import kotlin.math.floor

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class RecommendedPresenter(_view: RecommendedContract.View?): RecommendedContract.Presenter {

    private var recipes = arrayListOf<Recipe>()
    private var ingredients = arrayListOf<Ingredient>()
    private var db: FirebaseFirestore? = null
    private val recommendedView = _view

    init {
        if(_view != null) {
            db = FirebaseFirestore.getInstance()
        }

        getRecommendedRecipesFromDatabase()
    }

    override fun getRecipeList(): ArrayList<Recipe> {
        return recipes
    }

    private fun getRecommendedRecipesFromDatabase() {
        val ingredientsRef = db?.collection("ingredients")
        ingredientsRef?.get()?.addOnSuccessListener { document ->
            if(document != null) {
                val data = document.toObjects(Ingredient::class.java)
                ingredients.addAll(data)
                db?.collection("recipes")?.get()?.addOnSuccessListener { response ->
                    if(response != null) {
                        val dataParse = response.toObjects(Recipe::class.java)
                        val tempRecipes = arrayListOf<Recipe>()
                        dataParse.forEach {
                            tempRecipes.add(it)
                        }
                        checkIfContainsIngredients(tempRecipes)
                        recommendedView?.setRecipeData(recipes.toList())
                    }
                }
            }
        }
    }

    private fun checkIfContainsIngredients(recipes: ArrayList<Recipe>) {
        val myIngredients = UserService.getMyIngredients()

        for(myIngredient in myIngredients) {
            var foundRecipes = 0.0;
            for(recipe in recipes) {
                foundRecipes = 0.0;
                val totalIngredients = recipe.ingredients.size
                recipe.ingredients.forEach {
                    if(it.name.toLowerCase() == myIngredient.name.toLowerCase()) {
                        foundRecipes++
                    }
                }

                recipe.percentMatch = calculatePercentMatch(foundRecipes, totalIngredients).toString()
                if(foundRecipes > 0) {
                    this.recipes.add(recipe)
                }
            }
        }
    }

    fun calculatePercentMatch(foundRecipes: Double, total: Int): Int {
        return floor(foundRecipes/total * 100).toInt()
    }
}