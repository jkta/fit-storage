package com.jkta.fitstorage.main.mainNavigation.settings

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

interface SettingsContract {

    interface View {

    }

    interface Presenter {
        fun signOut()
    }
}