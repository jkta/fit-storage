package com.jkta.fitstorage.main.authorization.register

import com.google.firebase.auth.AuthResult
import com.jkta.fitstorage.models.AuthResponse
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action

/**
 * Created by Joanna Kuta on 31.01.2020.
 */
interface RegisterContract {

    interface View {

    }

    interface Presenter {
        fun registerUser(email: String, password: String, passwordRepeat: String): Observable<AuthResponse>
        fun checkIfPasswordsMatch(password: String, passwordRepeat: String): Boolean
    }
}