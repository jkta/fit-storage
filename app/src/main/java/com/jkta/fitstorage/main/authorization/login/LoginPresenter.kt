package com.jkta.fitstorage.main.authorization.login

import com.google.firebase.auth.FirebaseAuth
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Resources
import com.jkta.fitstorage.main.authorization.AuthContract
import com.jkta.fitstorage.models.AuthResponse
import com.jkta.fitstorage.services.UserService
import io.reactivex.Observable

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class LoginPresenter(_view: LoginContract.View): LoginContract.Presenter {

    private val firebaseAuth = FirebaseAuth.getInstance()
    private val view = _view

        override fun loginUserWithEmailAndPassword(
        email: String,
        password: String
    ): Observable<AuthResponse> {
        return Observable.create { emitter ->
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener {
                emitter.onNext(AuthResponse(isSuccess = true, message = Resources.string(R.string.auth_login_success)))
                UserService.initUserFromFirebaseAuth()
            }.addOnFailureListener {
                emitter.onError(Throwable(it.message))
            }
        }
    }
}