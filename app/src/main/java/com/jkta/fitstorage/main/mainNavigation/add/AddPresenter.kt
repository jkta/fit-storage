package com.jkta.fitstorage.main.mainNavigation.add

import com.google.firebase.firestore.FirebaseFirestore
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe
import com.jkta.fitstorage.services.UserService
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class AddPresenter(_view: AddContract.View): AddContract.Presenter {

    val view  = _view
    private val db = FirebaseFirestore.getInstance()
    private var ingredientList: ArrayList<Ingredient> = arrayListOf()

    override fun getIngredientsList(): ArrayList<Ingredient> {
        return ingredientList
    }

    override fun addIngredientToList(name: String, amount: Double, unit: String) {
        ingredientList.add(Ingredient(name, amount, unit))
    }

    override fun saveRecipe(title: String, recipeDetails: String) {
        val recipe = Recipe(title, ingredientList, recipeDetails, UserService.getUserUid(), UserService.getUserName(), Date())

        if(recipe.ingredients.isNotEmpty()) {
            saveInRemoteDatabase(recipe)
        } else {
            view.showToastMessage("Please add ingredients!")
        }
    }

    private fun saveInRemoteDatabase(recipe: Recipe) {
        db.collection("recipes").add(recipe).addOnSuccessListener {documentReference ->
            val ingredientsCollection = db.collection("recipes")
                .document(documentReference.id)
                .collection("ingredients")
            view.showToastMessage("Saved successfully")
        }.addOnFailureListener {
            view.showToastMessage("Error " + it.localizedMessage)
        }
    }
}