package com.jkta.fitstorage.main.authorization

import com.google.firebase.auth.FirebaseAuth
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Resources
import com.jkta.fitstorage.models.AuthResponse
import io.reactivex.Observable

/**
 * Created by Joanna Kuta on 30.01.2020.
 */

class AuthPresenter(_view: AuthContract.View): AuthContract.Presenter {

    private val firebaseAuth = FirebaseAuth.getInstance()
    private val view = _view


}