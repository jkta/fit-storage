package com.jkta.fitstorage.main.authorization.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Router
import com.jkta.fitstorage.helpers.toast
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_authentication_login.*

/**
 * Created by Joanna Kuta on 30.01.2020.
 */

class LoginFragment : Fragment(), LoginContract.View {

    private var presenter: LoginPresenter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter = LoginPresenter(this)
        return inflater.inflate(R.layout.fragment_authentication_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLoginClicked()
    }

    private fun setLoginClicked() {
        loginButton.setOnClickListener {
            presenter?.loginUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())!!.subscribeBy(
                onNext = {
                    Router.goToMainActivity()
                },
                onError = {
                    toast(it.message!!)
                }
            )
        }
    }
}