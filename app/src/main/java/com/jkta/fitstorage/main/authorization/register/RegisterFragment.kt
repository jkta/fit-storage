package com.jkta.fitstorage.main.authorization.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Router
import com.jkta.fitstorage.helpers.toast
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_authentication_register.*

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class RegisterFragment: Fragment(), RegisterContract.View {

    private var presenter: RegisterPresenter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        presenter = RegisterPresenter(this)
        return inflater.inflate(R.layout.fragment_authentication_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRegisterClicked()
    }

    private fun setRegisterClicked() {
        registerButton.setOnClickListener {
            presenter?.registerUser(registerEmailEditText.text.toString(), registerPasswordEditText.text.toString(), registerPasswordRepeatEditText.text.toString())!!.subscribeBy(
                onNext = {
                    Router.goToLoginActivity()
                },
                onError = {
                    toast(it.message!!)
                }
            )
        }
    }
}