package com.jkta.fitstorage.main.authorization.alternativeLogin

/**
 * Created by Joanna Kuta on 31.01.2020.
 */
interface AlternativeLoginContract {

    interface View {

    }

    interface  Presenter {

    }
}