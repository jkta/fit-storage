package com.jkta.fitstorage.main.mainNavigation.storage

import com.google.firebase.firestore.FirebaseFirestore
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class StoragePresenter(_view: StorageContract.View): StorageContract.Presenter {

    private var recipes = arrayListOf<Recipe>()
    private val storageView = _view
    private val db = FirebaseFirestore.getInstance()

    init {
        getRecipesFromDatabase()
    }

    override fun getRecipeList(): ArrayList<Recipe> {
        return recipes
    }

    private fun getRecipesFromDatabase() {
        db.collection("recipes").get().addOnSuccessListener { document ->
            if(document != null) {
                val data = document.toObjects(Recipe::class.java)
                recipes.addAll(data)
                storageView.setRecipeData(recipes.toList())
            }
        }
    }
}