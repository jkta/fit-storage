package com.jkta.fitstorage.main.mainNavigation.recommended

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jkta.fitstorage.R
import com.jkta.fitstorage.adapters.HomeHorizontalListAdapter
import com.jkta.fitstorage.adapters.HomeVerticalRecommendedAdapter
import com.jkta.fitstorage.models.Recipe
import kotlinx.android.synthetic.main.fragment_main_home.*
import kotlinx.android.synthetic.main.fragment_main_recommended.*

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class RecommendedFragment: Fragment(), RecommendedContract.View {

    private var presenter: RecommendedPresenter? = null
    private var adapter: HomeVerticalRecommendedAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_recommended, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = RecommendedPresenter(this)
        setupRecycler()
    }

    override fun setupRecycler() {
        adapter = HomeVerticalRecommendedAdapter(context!!, presenter!!.getRecipeList())
        recommendedRecycler.setHasFixedSize(true)
        recommendedRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recommendedRecycler.adapter = adapter
        recommendedRecycler.adapter!!.notifyDataSetChanged()
    }

    override fun setRecipeData(recipeList: List<Recipe>) {
        adapter!!.foodList.clear()
        adapter!!.foodList.addAll(recipeList)
        adapter!!.notifyDataSetChanged()
    }
}