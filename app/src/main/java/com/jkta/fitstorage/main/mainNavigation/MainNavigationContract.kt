package com.jkta.fitstorage.main.mainNavigation

import androidx.fragment.app.Fragment

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

interface MainNavigationContract {

    interface View {
        fun setupFragment(fragment: Fragment, backstack: Boolean = false)
        fun setupBottomNavigation()
    }

    interface Presenter {

    }
}