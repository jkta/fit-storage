package com.jkta.fitstorage.main.mainNavigation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jkta.fitstorage.R
import com.jkta.fitstorage.adapters.HomeHorizontalListAdapter
import com.jkta.fitstorage.adapters.HomeVerticalRecommendedAdapter
import com.jkta.fitstorage.adapters.IngredientAddedListener
import com.jkta.fitstorage.models.Ingredient
import com.jkta.fitstorage.models.Recipe
import kotlinx.android.synthetic.main.fragment_main_home.*

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class HomeFragment: Fragment(), HomeContract.View, IngredientAddedListener {

    private var presenter: HomePresenter? = null
    private var homeRecommendedAdapter: HomeVerticalRecommendedAdapter? = null
    private var homeHorizontalAdapter: HomeHorizontalListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = HomePresenter(this)
        setupRecycler()
        setupHorizontalRecycler()
    }

    override fun setupRecycler() {
        homeHorizontalAdapter = HomeHorizontalListAdapter(context!!, presenter!!.getIngredientList(), this)
        foodHorizontalRecycler.setHasFixedSize(true)
        foodHorizontalRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        foodHorizontalRecycler.adapter = homeHorizontalAdapter
        foodHorizontalRecycler.adapter!!.notifyDataSetChanged()
    }

    override fun setupHorizontalRecycler() {
        homeRecommendedAdapter = HomeVerticalRecommendedAdapter(context!!, presenter!!.getRecipeList())
        recentlyAddedRecyclerView.setHasFixedSize(true)
        recentlyAddedRecyclerView.layoutManager = LinearLayoutManager(context)
        recentlyAddedRecyclerView.adapter = homeRecommendedAdapter
        recentlyAddedRecyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun setRecipeData(recipeList: List<Recipe>) {
        homeRecommendedAdapter!!.foodList.clear()
        homeRecommendedAdapter!!.foodList.addAll(recipeList)
        homeRecommendedAdapter!!.notifyDataSetChanged()
    }

    override fun setIngredientData(ingredientList: List<Ingredient>) {
        homeHorizontalAdapter!!.ingredientList.clear()
        homeHorizontalAdapter!!.ingredientList.addAll(ingredientList)
        homeHorizontalAdapter!!.notifyDataSetChanged()
    }

    override fun onAddListener() {
        presenter!!.reloadIngredientList()
    }
}