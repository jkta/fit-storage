package com.jkta.fitstorage.main.mainNavigation.recommended

import com.jkta.fitstorage.models.Recipe

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

interface RecommendedContract {

    interface View {
        fun setupRecycler()
        fun setRecipeData(recipeList: List<Recipe>)
    }

    interface Presenter {
        fun getRecipeList(): ArrayList<Recipe>
    }
}