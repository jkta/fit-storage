package com.jkta.fitstorage.main.authorization.alternativeLogin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class AlternativeLoginFragment: Fragment(), AlternativeLoginContract.View {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_authentication_alternative, container, false)
    }
}