package com.jkta.fitstorage.main.authorization.register

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.jkta.fitstorage.R
import com.jkta.fitstorage.helpers.Resources
import com.jkta.fitstorage.models.AuthResponse
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

/**
 * Created by Joanna Kuta on 31.01.2020.
 */

class RegisterPresenter(_view: RegisterContract.View): RegisterContract.Presenter {

    private val firebaseAuth = FirebaseAuth.getInstance()

    override fun registerUser(email: String, password: String, passwordRepeat: String): Observable<AuthResponse> {
        return Observable.create<AuthResponse> { emitter ->
            if(checkIfPasswordsMatch(password, passwordRepeat)) {
                firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener {
                    emitter.onNext(AuthResponse(isSuccess = true, message =  Resources.string(R.string.auth_register_success)))
                }.addOnFailureListener {
                    emitter.onError(Throwable(it.message))
                }
            } else {
                emitter.onError(Throwable(Resources.string(R.string.auth_passwords_no_match)))
            }
        }
    }

    override fun checkIfPasswordsMatch(password: String, passwordRepeat: String): Boolean {
        return password == passwordRepeat
    }
}