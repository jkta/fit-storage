package com.jkta.fitstorage.main.mainNavigation.add

import com.jkta.fitstorage.models.Ingredient

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

interface AddContract {

    interface View {
        fun setupRecycler()
        fun showToastMessage(message: String)
    }

    interface Presenter {
        fun getIngredientsList(): ArrayList<Ingredient>
        fun addIngredientToList(name: String, amount: Double, unit: String)
        fun saveRecipe(title: String, recipeDetails: String)
    }
}