package com.jkta.fitstorage.main.mainNavigation.recipeDetails

import com.jkta.fitstorage.models.Recipe

/**
* Created by Joanna Kuta on 31/03/2020
*/

class RecipeDetailsPresenter(_view: RecipeDetailsContract.View): RecipeDetailsContract.Presenter {
    val view = _view
}