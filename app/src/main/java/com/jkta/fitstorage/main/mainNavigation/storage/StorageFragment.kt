package com.jkta.fitstorage.main.mainNavigation.storage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jkta.fitstorage.R
import com.jkta.fitstorage.adapters.HomeVerticalRecommendedAdapter
import com.jkta.fitstorage.models.Recipe
import kotlinx.android.synthetic.main.fragment_main_recommended.*
import kotlinx.android.synthetic.main.fragment_main_storage.*

/**
 * Created by Joanna Kuta on 07.02.2020.
 */

class StorageFragment: Fragment(), StorageContract.View {

    private var presenter: StoragePresenter? = null
    private var homeRecommendedAdapter: HomeVerticalRecommendedAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_storage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = StoragePresenter(this)
        setupRecipeRecycler()
    }

    override fun setRecipeData(recipeList: List<Recipe>) {
        homeRecommendedAdapter!!.foodList.clear()
        homeRecommendedAdapter!!.foodList.addAll(recipeList)
        homeRecommendedAdapter!!.notifyDataSetChanged()
    }

    private fun setupRecipeRecycler() {
        homeRecommendedAdapter = HomeVerticalRecommendedAdapter(context!!, presenter!!.getRecipeList())
        storageRecycler.setHasFixedSize(true)
        storageRecycler.layoutManager = LinearLayoutManager(context)
        storageRecycler.adapter = homeRecommendedAdapter
        storageRecycler.adapter!!.notifyDataSetChanged()
    }
}