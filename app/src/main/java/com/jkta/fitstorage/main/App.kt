package com.jkta.fitstorage.main

import android.app.Application
import android.content.Context
import com.jkta.fitstorage.services.UserService

/**
 * Created by Joanna Kuta on 01.02.2020.
 */

class App: Application() {

    init {
        instance = this
    }

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        UserService.initUserFromFirebaseAuth()
    }
}