package com.jkta.fitstorage.main.mainNavigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.jkta.fitstorage.R
import com.jkta.fitstorage.main.mainNavigation.add.AddFragment
import com.jkta.fitstorage.main.mainNavigation.home.HomeFragment
import com.jkta.fitstorage.main.mainNavigation.recommended.RecommendedFragment
import com.jkta.fitstorage.main.mainNavigation.settings.SettingsFragment
import com.jkta.fitstorage.main.mainNavigation.storage.StorageFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainNavigationActivity : AppCompatActivity(), MainNavigationContract.View {

    private var presenter: MainNavigationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = MainNavigationPresenter(this)
        setContentView(R.layout.activity_main)
        setupFragment(HomeFragment(), false)
        setupBottomNavigation()
    }

    override fun setupFragment(fragment: Fragment, backstack: Boolean) {
        val manager = supportFragmentManager
        val fragmentTransaction = manager.beginTransaction()
        if(backstack) {
            fragmentTransaction.addToBackStack(fragment.javaClass.name)
        }
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
        manager.executePendingTransactions()
    }

    override fun setupBottomNavigation() {
        navigation.setOnNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.navigation_home -> { setupFragment(HomeFragment())}
                R.id.navigation_recommended -> { setupFragment(RecommendedFragment())}
                R.id.navigation_storage -> { setupFragment(StorageFragment())}
                R.id.navigation_settings -> { setupFragment(SettingsFragment()) }
                R.id.navigation_add -> { setupFragment(AddFragment()) }
            }

            true
        }
    }

    override fun onBackPressed() {
        if(isCurrentFragmentHome())
            navigation.selectedItemId = R.id.navigation_home
        else
            super.onBackPressed()
    }

    private fun isCurrentFragmentHome(): Boolean {
        return navigation.selectedItemId != R.id.navigation_home
    }
}
