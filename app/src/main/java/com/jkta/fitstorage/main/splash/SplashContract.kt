package com.jkta.fitstorage.main.splash

/**
 * Created by Joanna Kuta on 19.02.2020.
 */
interface SplashContract {

    interface View {

    }

    interface Presenter {

    }
}