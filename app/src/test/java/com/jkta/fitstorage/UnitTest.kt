package com.jkta.fitstorage

import com.jkta.fitstorage.main.mainNavigation.recommended.RecommendedPresenter
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UnitTest {
    @Test
    fun percentage_isCorrect() {
        val presenter = RecommendedPresenter(null)
        val result = presenter.calculatePercentMatch(1.0, 2)
        val result2 = presenter.calculatePercentMatch(1.0, 3)
        val result3 = presenter.calculatePercentMatch(3.0, 3)
        assertTrue(result == 50)
        assertTrue(result2 == 33)
        assertTrue(result3 == 100)
    }
}
